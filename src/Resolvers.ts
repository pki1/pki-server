import { User } from "./entity/User";
import {
  Resolver,
  Query,
  Mutation,
  Arg,
  Field,
  ObjectType,
  Ctx,
  UseMiddleware
} from "type-graphql";
import { register, login } from "./utils/auth";
import { Block, getCertificates, Certificate } from "./blockchain";
import {
  createAccessToken,
  sendRefreshToken,
  createRefreshToken
} from "./utils/jwt";
import { isAuthenticated } from "./middleware/graphql/auth";
@ObjectType()
class LoginResponse {
  @Field()
  accessToken: string;
}
@ObjectType()
class CertificateS {
  @Field()
  version: string;
  @Field()
  user_identity: string;
  @Field()
  algorithm: string;
  @Field()
  validity: number;
  @Field()
  pub_key: string;
  @Field()
  issuer_id: string;
  @Field()
  usage: string;
  @Field()
  hashing_algorithm: string;
}

@Resolver()
export class Resolvers {
  @Query(() => String)
  hello() {
    return "hello";
  }

  @Query(() => [User])
  users() {
    return User.find();
  }
  @Query(() => String)
  @UseMiddleware(isAuthenticated)
  me() {
    return "test";
  }
  @Mutation(() => [CertificateS])
  @UseMiddleware(isAuthenticated)
  searchCertificates(
    @Arg("username") username: string,
    @Ctx() { blockchain }: any
  ) {
    const certificates = getCertificates(blockchain, username);
    console.log(`searchCertificates: ${username} certificates were searched`);
    return certificates;
  }

  @Query(() => [CertificateS])
  @UseMiddleware(isAuthenticated)
  myCertificates(@Ctx() { payload, blockchain }: any) {
    const { username } = payload;
    const certificates = getCertificates(blockchain, username);
    console.log(`myCertificates: sent certificates to ${username}`);
    return certificates;
  }
  @Query(() => [CertificateS])
  @UseMiddleware(isAuthenticated)
  allCertificates(@Ctx() { blockchain }: any) {
    const certificates = getCertificates(blockchain);
    return certificates;
  }
  @Mutation(() => Boolean)
  async register(
    @Arg("username") username: string,
    @Arg("password") password: string
  ) {
    return await register(username, password);
  }
  @Mutation(() => LoginResponse)
  async login(
    @Arg("username") username: string,
    @Arg("password") password: string,
    @Arg("rememberMe") rememberMe: boolean,
    @Ctx() { res }: any
  ) {
    const user = await login(username, password);

    sendRefreshToken(res, createRefreshToken(user, rememberMe));
    return {
      accessToken: createAccessToken(user)
    };
  }
  @Mutation(() => Boolean)
  async logout(@Ctx() { res }: any) {
    sendRefreshToken(res, "");
    return true;
  }

  @Mutation(() => Boolean)
  @UseMiddleware(isAuthenticated)
  async createCert(
    @Arg("algorithm") algorithm: string,
    @Arg("validity") validity: number,
    @Arg("pubKey") pubKey: string,
    @Arg("usage") usage: string,
    @Ctx() { blockchain, payload }: any
  ) {
    if (validity <= Date.now()) {
      throw new Error("Validity must be in the future");
    }
    const { username } = payload;
    try {
      const cert = new Certificate(
        "1.0",
        username,
        algorithm,
        validity,
        pubKey,
        "graphqlserver",
        usage,
        "SHA3"
      );
      console.log(`createCert: ${username} created cert`);
      blockchain.addBlock(new Block(Date.now(), username, cert));
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
    /*let bc = new BlockChain();
let cert = new Certificate(
  "0.1",
  "test",
  "ECDH",
  Date.now() + 10000,
  "pubkdowadkawodkawodkaw",
  "issuer",
  "ca_issuer",
  "SHA3"
).getObject();
bc.addBlock(new Block(1, Date.now(), "test", cert, "0"));*/
  }
}
