import { verify } from "jsonwebtoken";
import { MiddlewareFn } from "type-graphql";

const isAuthenticated: MiddlewareFn<any> = ({ context }, next) => {
  const authorization = context.req.headers["authorization"];
  if (!authorization) throw new Error("Not authenticated");
  try {
    const token = authorization.split(" ")[1];
    const payload = verify(
      token,
      process.env.accessTokenSecret || "testingAccess"
    );
    context.payload = payload as any;
  } catch (err) {
    console.log("middleware/graphql/auth/isAuthenticated: ", err);
    throw new Error("Not authenticated");
  }
  return next();
};

export { isAuthenticated };
