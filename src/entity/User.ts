import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";
//import { Certificate } from "./Certificate";
@ObjectType()
@Entity("users")
export class User extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;
  @Field()
  @Column()
  name: string;
  /*  @Field()
  @Column()
  age: number;
  @Field(() => Int)
  @Column({ unique: true })
  email: string;*/
  @Field()
  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  /*@OneToMany(
    type => Certificate,
    certificate => certificate.user
  )
  certificates: Certificate[];*/
}
