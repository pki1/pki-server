import * as crypto from "crypto-js";

class Certificate {
  version: string;
  user_identity: string;
  algorithm: string;
  validity: number;
  pub_key: string;
  issuer_id: string;
  usage: string;
  hashing_algorithm: string;
  constructor(
    version: string,
    user_identity: string,
    algorithm: string,
    validity: number,
    pub_key: string,
    issuer_id: string,
    usage: string,
    hashing_algorithm: string
  ) {
    this.version = version;
    this.user_identity = user_identity;
    this.algorithm = algorithm;
    this.validity = validity;
    this.pub_key = pub_key;
    this.issuer_id = issuer_id;
    this.usage = usage;
    this.hashing_algorithm = hashing_algorithm;
  }

  getObject() {
    return {
      version: this.version,
      user_identity: this.user_identity,
      algorithm: this.algorithm,
      validity: this.validity,
      pub_key: this.pub_key,
      issuer_id: this.issuer_id,
      hashing_algorithm: this.hashing_algorithm,
      usage: this.usage
    };
  }
}

class Block {
  timestamp: number;
  user: string;
  certificate: Object;
  hash: string;
  nonce: number;
  previousHash: string;
  constructor(timestamp: number, user: string, certificate: Object) {
    this.timestamp = timestamp;
    this.user = user;
    this.certificate = certificate;
    this.previousHash = "";
    this.hash = this.calculateHash();
    this.nonce = 0;
  }

  calculateHash() {
    return crypto
      .SHA3(
        this.previousHash +
          String(this.timestamp) +
          this.user +
          JSON.stringify(this.certificate) +
          String(this.nonce)
      )
      .toString();
  }

  mineBlock(difficulty: number) {
    while (
      this.hash.substring(0, difficulty) !== Array(difficulty + 1).join("0")
    ) {
      this.nonce++;
      this.hash = this.calculateHash();
    }
  }
}

class BlockChain {
  chain: Array<Block>;
  difficulty: number;
  constructor(difficulty: number) {
    this.chain = [this.createGenesisBlock()];
    this.difficulty = difficulty;
  }

  createGenesisBlock() {
    return new Block(Date.now(), "Genesis Block", {});
  }

  getLatestBlock() {
    return this.chain[this.chain.length - 1];
  }

  addBlock(newBlock: Block) {
    newBlock.previousHash = this.getLatestBlock().hash;
    newBlock.mineBlock(this.difficulty);
    this.chain.push(newBlock);
  }
}

const isEmpty = (obj: object) => {
  for (let x in obj) {
    return false;
  }
  return true;
};

const getCertificates = (blockchain: BlockChain, username?: string) => {
  return blockchain.chain
    .filter(block =>
      username
        ? block.user === username && !isEmpty(block.certificate)
        : !isEmpty(block.certificate)
    )
    .map(({ certificate }) => certificate);
};

//export { BlockChain };
export { Certificate, getCertificates, Block, BlockChain };
/*
let bc = new BlockChain(3);
let cert = new Certificate(
  "0.1",
  "test",
  "ECDH",
  Date.now() + 10000,
  "pubkdowadkawodkawodkaw",
  "issuer",
  "ca_issuer",
  "SHA3"
).getObject();
bc.addBlock(new Block(Date.now(), "test", cert, "0")); 
console.log(bc);*/
